package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Adresse {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column
	private String rue;
	@Column
	private String ville;
	
	public Adresse(int id) {
		
		this.id = id;
	}
	public Adresse(String rue, String ville) {
		super();
		this.rue = rue;
		this.ville = ville;
	}
	
	
	
}
