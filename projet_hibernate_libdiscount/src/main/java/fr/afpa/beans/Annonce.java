package fr.afpa.beans;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Annonce {

	private int id;
	private String titre;
	private int quantite;
	private float prix;
	private float prixTotal;
	private float remise;
	private String maisonEdition;
	private String isbn;
	private LocalDate dateEdition;
	private LocalDate dateAnnonce;
}
