package fr.afpa.beans;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Connexion {

	@Column
	private String login;
	@Column
	private String mdp;
	
	private int idUser;
	
}
