package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Connexion;
import fr.afpa.beans.Utilisateur;


public class DaoUtilisateur {
	
	/**
	 * 
	 * @param log
	 * @return
	 */
	public static Utilisateur recupUser(Connexion log) {
		
		ResultSet listUtilisateur = null;
		Utilisateur utilisateur = null;
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM utilisateur where iduser = " + String.valueOf(log.getIdUser());
			
			Statement stServices = conn.createStatement();
			
			listUtilisateur = stServices.executeQuery(strQuery);
			
			
			while (listUtilisateur.next()) {
					utilisateur = new Utilisateur();
					utilisateur.setId(listUtilisateur.getInt(1));
					utilisateur.setNom(listUtilisateur.getString("nom"));
					utilisateur.setPrenom(listUtilisateur.getString("prenom"));
					utilisateur.setMail(listUtilisateur.getString("mail"));
					utilisateur.setNomLibrairie(listUtilisateur.getString("nomlibrairie"));
					utilisateur.setTelephone(listUtilisateur.getString("telephone"));

					Adresse adresse = new Adresse(listUtilisateur.getString("adresselibrairie"),
							listUtilisateur.getString("ville"));
					
					utilisateur.setAdresse(adresse);
					
				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}
	
	/**
	 * 
	 * @param log
	 * @param user
	 * @return
	 */
	public static Utilisateur recupUserByName(Connexion log, Utilisateur user) {
		
		ResultSet listUtilisateur = null;
		Utilisateur utilisateur = null;
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			
			String strQuery = "SELECT * FROM utilisateur where iduser = " + String.valueOf(log.getIdUser() 
					+ " or nom = '" + user.getNom() + "' and prenom = '" + user.getPrenom() + "'");
			
			Statement stServices = conn.createStatement();
			
			listUtilisateur = stServices.executeQuery(strQuery);
			
			
			while (listUtilisateur.next()) {
					utilisateur = new Utilisateur();
					utilisateur.setId(listUtilisateur.getInt(1));
					utilisateur.setNom(listUtilisateur.getString("nom"));
					utilisateur.setPrenom(listUtilisateur.getString("prenom"));
					utilisateur.setMail(listUtilisateur.getString("mail"));
					utilisateur.setNomLibrairie(listUtilisateur.getString("nomlibrairie"));
					utilisateur.setTelephone(listUtilisateur.getString("telephone"));

					Adresse adresse = new Adresse(listUtilisateur.getString("adresselibrairie"),
							listUtilisateur.getString("ville"));
					
					utilisateur.setAdresse(adresse);
					
				}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return utilisateur;
	}
	
	/**
	 * 
	 * @param user
	 */
	public static void insertInto(Utilisateur user) {
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			conn.setAutoCommit(false);
			
			String strQuery = "insert into utilisateur (iduser ,nom,prenom,mail ,telephone ,nomlibrairie ,adresselibrairie, ville)"
					+ "values ( nextval('incrementuser') ,"
					+ "'" + user.getNom() + "',"
					+ "'" + user.getPrenom() + "',"
					+ "'" + user.getMail() + "',"
					+ "'" + user.getTelephone() + "',"
					+ "'" + user.getNomLibrairie() + "',"
					+ "'" + user.getAdresse().getRue() + "',"
					+ "'" + user.getAdresse().getVille() +"')";
			
			Statement stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			
			stServices.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * @param user
	 */
	public static void updateCompte(Utilisateur user) {
		
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			conn.setAutoCommit(false);
			
			String strQuery = "UPDATE utilisateur SET "
					+ "nom = '" + user.getNom() + "',"
					+ "prenom = '" + user.getPrenom() + "',"
					+ "mail = '" + user.getMail() + "',"
					+ "telephone = '" + user.getTelephone() + "'"
					+ " where iduser = " + String.valueOf(user.getId());
			
			Statement stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			
			stServices.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * @param user
	 */
	public static void deleteCompte(Utilisateur user) {
		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/libdata", "libadmin", "libadmin");
			conn.setAutoCommit(false);
			
			String strQuery = "DELETE FROM utilisateur where iduser = " + user.getId();
			
			Statement stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			
			stServices.close();
			conn.commit();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
