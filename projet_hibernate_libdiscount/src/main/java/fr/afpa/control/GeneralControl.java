package fr.afpa.control;


import fr.afpa.beans.Connexion;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.DaoAnnonce;
import fr.afpa.dao.DaoLog;
import fr.afpa.dao.DaoUtilisateur;
import fr.afpa.service.ServiceAnnonce;
import fr.afpa.service.ServiceLog;
import fr.afpa.service.ServiceUtilisateur;
import fr.afpa.vue.Menu;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeneralControl {
	
	private static Utilisateur utilisateur;
	
	/**
	 * Methode de lancement du projet
	 */
	public static void lancement() {
		Menu.accueil();
	}
	
	/**
	 * 
	 * @param login
	 * @param mdp
	 * @return
	 */
	public static boolean login(String login, String mdp) {
		if (ServiceLog.login(login, mdp)) {
			System.out.println("Bienvenu " 
			+ GeneralControl.getUtilisateur().getPrenom() + " !");
			Connexion connexion = new Connexion();
			connexion.setIdUser(utilisateur.getId());
			connexion.setLogin(login);
			connexion.setMdp(mdp);
			utilisateur.setLogin(connexion);
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param login
	 * @param mdp
	 */
	public static void listLogin(String login, String mdp) {
		ServiceLog.allLogin();
	}
	
	
	/**
	 * 
	 */
	public static void posterAnnonce() {
		DaoAnnonce.insertInto(ServiceAnnonce.posterAnnonce());
		System.out.println("Annonce post�e par " + utilisateur.getPrenom() +" !");
	}
	
	/**
	 * 
	 */
	public static void creerCompte() {
		DaoUtilisateur.insertInto(ServiceUtilisateur.creerCompte());
		Utilisateur userTemp = DaoUtilisateur.recupUserByName(utilisateur.getLogin(), utilisateur);
		DaoLog.insertInto(utilisateur ,userTemp);
		System.out.println("Compte utilisateur cr�� ! : " + utilisateur.getPrenom());
	}
	
	/**
	 * 
	 */
	public static void listerSelfAnnonces() {
		ServiceAnnonce.listerAnnonces(DaoAnnonce.recupSelfAnnonces(utilisateur));
	}
	
	public static void listAnnonceFiltre() {
		ServiceAnnonce.listerFiltre();
		
	}
	
	/**
	 * 
	 */
	public static void modifierAnnonce() {
		DaoAnnonce.updateModif(ServiceAnnonce.modifierAnnonce());
	}
	
	/**
	 * 
	 */
	public static void supprimerAnnonce() {
		DaoAnnonce.deleteAnnonce(ServiceAnnonce.supprimerAnnonce());
		
	}
	
	
	/**
	 * 
	 */
	public static void consulterInfos() {
		ServiceUtilisateur.consulterInfos(utilisateur);
	}
	
	/**
	 * 
	 */
	public static void modifierCompte() {
		Utilisateur user = ServiceUtilisateur.modifierCompte(utilisateur);
		if (user != null) {		
			DaoUtilisateur.updateCompte(user);
			DaoLog.updateLogin(user);
			System.out.println("Modification Enregistr�es !");
		}
	}
	
	/**
	 * 
	 */
	public static void supprimerCompte() {
		DaoLog.deleteLogin(utilisateur);
		DaoUtilisateur.deleteCompte(utilisateur);
		System.out.println("Compte Supprim� !");
	}
	
	
	
	// Get & Set de l'utilisateur actuel
	
	
	/**
	 * 
	 * @return
	 */
	public static Utilisateur getUtilisateur() {
		return utilisateur;
	}

	/**
	 * 
	 * @param utilisateur
	 */
	public static void setUtilisateur(Utilisateur utilisateur) {
		GeneralControl.utilisateur = utilisateur;
	}






	
	
}
